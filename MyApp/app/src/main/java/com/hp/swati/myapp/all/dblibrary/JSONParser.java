package com.hp.swati.myapp.all.dblibrary;

/**
 * Created by hp on 01-03-2016.
 */

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class JSONParser {

    /*static InputStream is = null;
    static JSONObject jObj = null;
    static String json = "";

    // constructor
    public JSONParser() {

    }

    public String getJSONFromUrl(String url, List params) {*/


    static String json = "";
    String charset = "UTF-8";
    HttpURLConnection conn;
    DataOutputStream wr;
   static StringBuilder result;
    URL urlObj;
   static JSONObject jObj = null;
    StringBuilder sbParams;
    String paramsString;

    // constructor
    public JSONParser() {

    }

    public JSONObject getJSONFromUrl(String url, List<NameValuePair> params) {

        sbParams = new StringBuilder();
       int i = 0;
        for ( NameValuePair key : params)  {
          //  String key = params.get(i);  //for (String key : params.keySet())
            try {
                if (i != 0) {
                    sbParams.append("&");
                }
                /*result.append(URLEncoder.encode(pair.getName(), "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(pair.getValue(), "UTF-8"));*/
                /*solution : http://stackoverflow.com/questions/9767952/how-to-add-parameters-to-httpurlconnection-using-post*/
                sbParams.append(key.getName()).append("=")
                        .append(URLEncoder.encode( key.getValue(), charset));//encode(params.get(key), charset)

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            i++;
        }

       /* if (method.equals("POST")) {*/
            // request method is POST
            try {
                urlObj = new URL(url);

                conn = (HttpURLConnection) urlObj.openConnection();

                conn.setDoOutput(true);

                conn.setRequestMethod("POST");

                conn.setRequestProperty("Accept-Charset", charset);

                conn.setReadTimeout(10000);
                conn.setConnectTimeout(15000);

                conn.connect();

                paramsString = sbParams.toString();

                wr = new DataOutputStream(conn.getOutputStream());
                wr.writeBytes(paramsString);
                wr.flush();
                wr.close();

            } catch (IOException e) {
                e.printStackTrace();
            }





        // Making HTTP request
       /* try {
            // defaultHttpClient
            DefaultHttpClient httpClient = new DefaultHttpClient();
            HttpPost httpPost = new HttpPost(url);
            httpPost.setEntity(new UrlEncodedFormEntity(params));

            HttpResponse httpResponse = httpClient.execute(httpPost);
            HttpEntity httpEntity = httpResponse.getEntity();
            is = httpEntity.getContent();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }*/


        try{
            //Receive the response from the server
            InputStream in = new BufferedInputStream(conn.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            result = new StringBuilder();
            String line=null;
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }


            json= result.toString().trim();



            Log.d("JSON Parser", "result: " + json);

        }
        catch (IOException e) {
            e.printStackTrace();
            Log.e("JSONPARSER" , e.getMessage());
        }

        conn.disconnect();
        /*String json = null;
        json = sbParams.toString(); */

        // try parse the string to a JSON object
        try {
            jObj = new JSONObject(json);

        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

        // return JSON Object
        return jObj;
    }


        /*try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(
                    is, "iso-8859-1"), 8);
            StringBuilder sb = new StringBuilder();
            String line = null;
            while ((line = reader.readLine()) != null) {
                sb.append(line + "n");
            }
            is.close();
            json = sb.toString();
            Log.e("JSON", json);
        } catch (Exception e) {
            Log.e("Buffer Error", "Error converting result " + e.toString());
        }*/

       /* BufferedReader bufferedReader = null;
        try {
            URL url2 = new URL(url);
            HttpURLConnection con = (HttpURLConnection) url2.openConnection();
            StringBuilder sb = new StringBuilder();

            bufferedReader = new BufferedReader(new InputStreamReader(con.getInputStream()));

            //String json = null;
            while((json = bufferedReader.readLine())!= null){
                sb.append(json+"\n");
            }

            return sb.toString().trim();
            Log.e("JSON",json);

        }catch(Exception e){
            return null;
        }

        // try parse the string to a JSON object
        try {
            jObj = new JSONObject(json);
        } catch (JSONException e) {
            Log.e("JSON Parser", "Error parsing data " + e.toString());
        }

        // return JSON String
        return jObj;

    }*/
}
/*public class JSONParser {
}*/
